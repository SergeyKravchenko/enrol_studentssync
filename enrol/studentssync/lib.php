<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Students to courses sync plugin.
 *
 * @package    enrol_studentssync
 * @copyright  2017, Sergey Kravchenko <kravchenko.sgv@gmail.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

global $CFG;
require_once($CFG->dirroot.'/enrol/flatfile/lib.php');
require_once($CFG->dirroot.'/cohort/lib.php');

/**
 * Students to courses sync plugin.
 * @author Sergey Kravchenko
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class enrol_studentssync_plugin extends enrol_flatfile_plugin {

    /**
     * Execute synchronisation.
     * @param progress_trace
     * @return int exit code, 0 means ok, 2 means plugin disabled
     */
    public function sync(progress_trace $trace) {
        if (!enrol_is_enabled('studentssync')) {
            return 2;
        }

        $processed = false;

        $processed = $this->process_file($trace) || $processed;
        $processed = $this->process_buffer($trace) || $processed;
        $processed = $this->process_expirations($trace) || $processed;

        return 0;
    }

    /**
     * Process csv-file.
     * @param progress_trace $trace
     * @return bool true if any data processed, false if not
     */
    protected function process_file(progress_trace $trace) {
        global $CFG, $DB;

        // We may need more memory here.
        core_php_time_limit::raise();
        raise_memory_limit(MEMORY_HUGE);

        $filelocation = $this->get_config('location');
        if (empty($filelocation)) {
            // Default legacy location.
            $filelocation = "$CFG->dataroot/students.txt";
        }
        $disclosefile = $this->obfuscate_filepath($filelocation);

        if (!file_exists($filelocation)) {
            $trace->output("Students enrolments csv-file not found: $disclosefile");
            $trace->finished();
            return false;
        }
        $trace->output("Processing students csv-file enrolments from: $disclosefile ...");

        $content = file_get_contents($filelocation);

        if ($content !== false) {

            //$rolemap = $this->get_role_map($trace);

            //$content = core_text::convert($content, $this->get_config('encoding', 'utf-8'), 'utf-8');
            $content = str_replace("\r", '', $content);
            $content = explode("\n", $content);

            $line = 0;
            foreach($content as $fields) {
                $line++;

                if (trim($fields) === '') {
                    // Empty lines are ignored.
                    continue;
                }

                // Deal with different separators.
                if (strpos($fields, ',') !== false) {
                    $fields = explode(',', $fields);
                } else {
                    $fields = explode(';', $fields);
                }

                // If a line is incorrectly formatted ie does not have 5 comma separated fields then ignore it.
                if (count($fields) < 5 or count($fields) > 5) {
                    $trace->output("Line incorrectly formatted - ignoring $line", 1);
                    continue;
                }

                $fields[0] = trim($fields[0]);
                $fields[1] = trim($fields[1]);
                $fields[2] = trim($fields[2]);
                $fields[3] = trim($fields[3]);
                $fields[4] = trim($fields[4]);

                // Deal with quoted values - all or nothing, we need to support "' in idnumbers, sorry.
                if (strpos($fields[0], "'") === 0) {
                    foreach ($fields as $k=>$v) {
                        $fields[$k] = trim($v, "'");
                    }
                } else if (strpos($fields[0], '"') === 0) {
                    foreach ($fields as $k=>$v) {
                        $fields[$k] = trim($v, '"');
                    }
                }

                $trace->output("$line: $fields[0], $fields[1], $fields[2], $fields[3], $fields[4], $fields[5]", 1);

                if ($fields[0] === "lastname") {
                    continue;
                }

                // Check correct formatting of operation field.
                //if ($fields[0] !== "add" and $fields[0] !== "del") {
                //    $trace->output("Unknown operation in field 1 - ignoring line $line", 1);
                //    continue;
                //}

                // Check correct formatting of role field.
                //if (!isset($rolemap[$fields[1]])) {
                //    $trace->output("Unknown role in field2 - ignoring line $line", 1);
                //    continue;
                //}
                //$roleid = $rolemap[$fields[1]];

                //if (empty($fields[2]) or !$user = $DB->get_record("user", array("idnumber"=>$fields[2], 'deleted'=>0))) {
                //    $trace->output("Unknown user idnumber or deleted user in field 3 - ignoring line $line", 1);
                //    continue;
                //}

                //if (!$course = $DB->get_record("course", array("idnumber"=>$fields[3]))) {
                //    $trace->output("Unknown course idnumber in field 4 - ignoring line $line", 1);
                //    continue;
                //}

                //if ($fields[4] > $fields[5] and $fields[5] != 0) {
                //    $trace->output("Start time was later than end time - ignoring line $line", 1);
                //    continue;
                //}

                $this->process_records($trace, $fields);
            }

            unset($content);
        }

        //if (!unlink($filelocation)) {
        //    $eventdata = new \core\message\message();
        //    $eventdata->courseid          = SITEID;
        //    $eventdata->modulename        = 'moodle';
        //    $eventdata->component         = 'enrol_flatfile';
        //    $eventdata->name              = 'flatfile_enrolment';
        //    $eventdata->userfrom          = get_admin();
        //    $eventdata->userto            = get_admin();
        //    $eventdata->subject           = get_string('filelockedmailsubject', 'enrol_flatfile');
        //    $eventdata->fullmessage       = get_string('filelockedmail', 'enrol_flatfile', $filelocation);
        //    $eventdata->fullmessageformat = FORMAT_PLAIN;
        //    $eventdata->fullmessagehtml   = '';
        //    $eventdata->smallmessage      = '';
        //    message_send($eventdata);
        //    $trace->output("Error deleting enrolment file: $disclosefile", 1);
        //} else {
        //    $trace->output("Deleted enrolment file", 1);
        //}

        $trace->output("...finished students csv-file processing.");
        $trace->finished();

        return true;
    }

    /**
     * Process user enrolment line.
     *
     * @param progress_trace $trace
     * @param array $fields line fields
     * @param null $roleid
     * @param null $user
     * @param null $course
     * @param null $timestart
     * @param null $timeend
     * @param null $buffer_if_future
     */
    protected function process_records(progress_trace $trace, $fields, $roleid = null, $user = null, $course = null, $timestart = null, $timeend = null, $buffer_if_future = null) {
        global $CFG, $DB;

        if (!$DB->record_exists('user', array('idnumber' => $fields[4]))) {
            // Добавить пользователя
            $user = new stdClass();

            // Генерируем новый логин
            $user->username = $this->generate_login($fields[0], $fields[1], $fields[2], $fields[3], $fields[4]);

            $user->confirmed = 0;
            $user->password = md5($user->username);
            $user->email = $this->generate_email($user->username);
            $user->timecreated = time();
            $user->timemodified = time();
            $user->lastname = $fields[0];
            $user->firstname = $fields[1];
            $user->middlename = $fields[2];
            $user->idnumber = $fields[4];
            $user->mnethostid = $CFG->mnet_localhost_id;
            $user->lang = 'ru_utf8';

            $userid = $DB->insert_record('user', $user);
//        } else if (1) {
            // Если пользователь является вернувшимся из академического отпуска
            // Восстановить этого пользователя
        } else {
            // Обновить пользователя
            // Установить временный логин
            $user = $DB->get_record('user', array('idnumber' => $fields[4]));
            $user->username = "unnamed_user";
            $DB->update_record('user', $user);

            // Генерируем новый логин
            $user->username = $this->generate_login($fields[0], $fields[1], $fields[2], $fields[3], $fields[4]);

            $user->timecreated = time();
            $user->timemodified = time();
            $user->lastname = $fields[0];
            $user->firstname = $fields[1];
            $user->middlename = $fields[2];
            $user->idnumber = $fields[4];
            $DB->update_record('user', $user);
            $userid = $user->id;
        }

        // Определить номер группы
        preg_match('/\d+/', $fields[3], $cohortnum);

        // Идентифицировать когорту
        if (!$DB->record_exists('cohort', array("idnumber" => $cohortnum[0]))) {
            // Создать когорту
            $cohort = new stdClass();
            $cohort->contextid = context_system::instance()->id;
            $cohort->name = $fields[3];                             // Человеко-читаемое имя когорты
            $cohort->idnumber = $cohortnum[0];                         // Уникальный идентификатор когорты
            $cohort->description = '';                              // Длинное описание
            $cohort->descriptionformat = FORMAT_HTML;               // Формат описания
            $cohortid = cohort_add_cohort($cohort);
        } else {
            $cohortid = $DB->get_record('cohort', array("idnumber" => $cohortnum[0]))->id;
        }

        // Добавить пользователя в когорту
        cohort_add_member($cohortid, $userid);

        // Записать когорту на курсы
        // Создать резервную копию курса
        // Если группа не найдена
        // Создать группу в курсе
    }

    /**
     * Генерирует логин
     *
     * @param $ln - lastname
     * @param $fn - firstname
     * @param $mn - middlename
     * @param $cohort - cohort
     * @param $idnumber - idnumber
     * @return int|string - сгенерированный логин или -1 в случае ошибки
     */
    public function generate_login($ln, $fn, $mn, $cohort, $idnumber){
        global $DB;

        $helparray = array(1,1,1);

        $loginstart = $this->make_login($ln, $fn, $mn, $helparray);

        preg_match('/\d+/', $cohort, $cohortnum);
        $loginend = $cohortnum[0] . '-' . date('Y');

        $login = $loginstart . $loginend;

        $success = $loginstart != "" && !$DB->record_exists('user', array('username'=>$login));

        if ($success) return $login;

        $maxlength = mb_strlen($ln) + mb_strlen($fn) + mb_strlen($mn); // Максимальное количество букв в логине
        $length = 3;    // Длина логина, изначально 3 буквы - вообще надо брать сумму helparray
        $shift = true;  // Нужно ли осуществялть сдвиг?

        // Внешний цикл с увеличением чисел в массиве
        while (!$success && $length < $maxlength) {
            // Увеличим минимальную цифру - можно вынести в отдельную функцию
            if ($helparray[0] < mb_strlen($ln)) {
                $minnum = $helparray[0];
            }

            $minindex = 0; // Его положение
            for($i=1;$i<count($helparray); $i++){
                if($helparray[$i] < $minnum){
                    if (($i == 1 && $helparray[$i] + 1 <= mb_strlen($fn)) || ($i == 2 && $helparray[$i] + 1 <= mb_strlen($mn))){
                        $minnum = $helparray[$i];
                        $minindex = $i;
                    }
                }
            }

            if ($minindex == 0 && !($helparray[0] < mb_strlen($ln))) {
                $minnum++;
                $shift = false;
                continue;
            }

            $helparray[$minindex]++; // Увеличиваем цифру
            $length++;

            // Теперь прогоняем цифры по кругу
            $equal = $helparray[0] == $helparray[1] && $helparray[0] == $helparray[2];
            for($i=0;!$success && $i<count($helparray);$i++){
                if (($i && !$equal) || !$i) {
                    if ($shift || (!$shift && !$i)) {
                        $loginstart = $this->make_login($ln, $fn, $mn, $helparray);
                    }

                    $login = $loginstart . $loginend;

                    $success = $loginstart != "" && !$DB->record_exists('user', array('username'=>$login));

                    if ($shift && !$success) { // Сдвигаем массив (можно отдельной функцией)
                        $tmp = $helparray[count($helparray) - 1];
                        $arrtmp = $helparray;
                        for ($j = 0; $j < count($helparray) - 1; $j++) {
                            $helparray[$j + 1] = $arrtmp[$j];
                        }

                        $helparray[0] = $tmp;
                    }
                }
            }
        }

        if(!$success){ // Так и не нашлось свободной комбинации
            return -1; // Вернуть ошибку
        }

        return $login;
    }

    /**
     * Создать логин из 3 частей по заданному массиву
     *
     * @param $ln - lastname
     * @param $fn - firstname
     * @param $mn - middlename
     * @param $helparray
     * @return string
     */
    private function make_login($ln, $fn, $mn, $helparray){
        $res = "";

        // Если символов в строках хватает
        if (mb_strlen($ln) >= $helparray[0] && mb_strlen($fn) >= $helparray[1] && mb_strlen($mn) >= $helparray[2]){
            // Берем указанное число символов из каждой строки
            $res = $this->translit('ru', mb_substr($ln,0,$helparray[0],"UTF-8")) .
                $this->translit('ru', mb_substr($fn,0,$helparray[1],"UTF-8")) .
                $this->translit('ru', mb_substr($mn,0,$helparray[2],"UTF-8"));
        }

        return $res; // Если символов не хватало, то вернется пустая строка
    }

    /**
     * Генерирует email по заданному логину
     */
    public function generate_email($login)
    {
        $suffix = 'moodle.com';
        return $login . '@' . $suffix;
    }

    /**
     * Транслителировать строку в латиницу
     *
     * @param string $lang - двухбуквенный код языка
     * @param string $string - строка
     * @param bool $small - перевод в нижний регистр
     * @return string
     */
    private function translit($lang, $string, $small = true)
    {
        // Формируем массив транслитерации
        if ( $lang === 'ru' )
        {
            $alphabet = array(
                'а' => 'a', 'А' => 'A', 'б' => 'b', 'Б' => 'B', 'в' => 'v', 'В' => 'V',
                'г' => 'g', 'Г' => 'G', 'д' => 'd', 'Д' => 'D', 'е' => 'e', 'Е' => 'E',
                'ё' => 'jo', 'Ё' => 'Jo', 'ж' => 'zh', 'Ж' => 'Zh', 'з' => 'z', 'З' => 'Z',
                'и' => 'i', 'И' => 'I', 'й' => 'j', 'Й' => 'J', 'к' => 'k', 'К' => 'K',
                'л' => 'l', 'Л' => 'L', 'м' => 'm', 'М' => 'M', 'н' => 'n', 'Н' => 'N',
                'о' => 'o', 'О' => 'O', 'п' => 'p', 'П' => 'P', 'р' => 'r', 'Р' => 'R',
                'с' => 's', 'С' => 'S', 'т' => 't', 'Т' => 'T', 'у' => 'u', 'У' => 'U',
                'ф' => 'f', 'Ф' => 'F', 'х' => 'h', 'Х' => 'h', 'ц' => 'c', 'Ц' => 'C',
                'ч' => 'ch', 'Ч' => 'Ch', 'ш' => 'sh', 'Ш' => 'Sh', 'щ' => 'shh', 'Щ' => 'Shh',
                'ъ' => '', 'Ъ' => '', 'ы' => 'y', 'Ы' => 'Y', 'ь' => "", 'Ь' => "",
                'э' => 'e', 'Э' => 'E', 'ю' => 'ju', 'Ю' => 'Ju', 'я' => 'ja', 'Я' => 'Ja');
        }

        // Переводим в транслит
        $string = strtr($string, $alphabet);

        // Если требуется перевести в нижний регистр
        if ( $small )
        {
            $string = strtolower($string);
        }

        // Чтоб не было конфликтов перед обработкой убираем экранирование
        return addslashes($string);
    }
}