<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Students to courses sync plugin tests.
 *
 * @package    enrol_studentssync
 * @category   phpunit
 * @copyright  2017, Sergey Kravchenko <kravchenko.sgv@gmail.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();


class enrol_studentssync_lib_testcase extends advanced_testcase {

    protected function enable_plugin() {
        $enabled = enrol_get_plugins(true);
        $enabled['studentssync'] = true;
        $enabled = array_keys($enabled);
        set_config('enrol_plugins_enabled', implode(',', $enabled));
    }

    /**
     * Генерация логина
     */
    public function test_generate_login() {
        $currentyear = date('Y');

        $this->resetAfterTest();

        $studentssyncplugin = enrol_get_plugin('studentssync');

        // akr
        $user1 = $this->getDataGenerator()->create_user(array('username'=>'akr166-'.$currentyear));
        // abkr
        $user2 = $this->getDataGenerator()->create_user(array('username'=>'abkr166-'.$currentyear));
        // aklr
        $user3 = $this->getDataGenerator()->create_user(array('username'=>'aklr166-'.$currentyear));
        // akrs
        $user4 = $this->getDataGenerator()->create_user(array('username'=>'akrs166-'.$currentyear));
        // abklr
        $user5 = $this->getDataGenerator()->create_user(array('username'=>'abklr166-'.$currentyear));
        // aklrs
        $user6 = $this->getDataGenerator()->create_user(array('username'=>'aklrs166-'.$currentyear));
        // abkrs
        $user7 = $this->getDataGenerator()->create_user(array('username'=>'abkrs166-'.$currentyear));
        // abklrs
        $user8 = $this->getDataGenerator()->create_user(array('username'=>'abklrs166-'.$currentyear));
        // abcklrs
        $user9 = $this->getDataGenerator()->create_user(array('username'=>'abcklrs166-'.$currentyear));
        // abklmrs
        $user10 = $this->getDataGenerator()->create_user(array('username'=>'abklmrs166-'.$currentyear));
        // abcklmrs
        $user11 = $this->getDataGenerator()->create_user(array('username'=>'abcklmrs166-'.$currentyear));
        // abcdklmrs
        $user12 = $this->getDataGenerator()->create_user(array('username'=>'abcdklmrs166-'.$currentyear));
        // abcdklmnrs
        $user13 = $this->getDataGenerator()->create_user(array('username'=>'abcdklmnrs166-'.$currentyear));
        // abcdklmnors
        $user14 = $this->getDataGenerator()->create_user(array('username'=>'abcdklmnors166-'.$currentyear));
        // abcdklmnoprs
        $username = $studentssyncplugin->generate_login('abcd', 'klmnop', 'rs', 'cohort-166');

        $this->assertEquals('abcdklmnoprs166-'.$currentyear, $username);

        // ацю - acju
        $user1 = $this->getDataGenerator()->create_user(array('username'=>'acju166-'.$currentyear));
        // абцю - abcju
        $user2 = $this->getDataGenerator()->create_user(array('username'=>'abcju166-'.$currentyear));
        // ацхю - achju
        $user3 = $this->getDataGenerator()->create_user(array('username'=>'achju166-'.$currentyear));
        // ацюя - acjuja
        $user4 = $this->getDataGenerator()->create_user(array('username'=>'acjuja166-'.$currentyear));
        // абцхю - abchju
        $user5 = $this->getDataGenerator()->create_user(array('username'=>'abchju166-'.$currentyear));
        // ацхюя - achjuja
        $user6 = $this->getDataGenerator()->create_user(array('username'=>'achjuja166-'.$currentyear));
        // абцюя - abcjuja
        $user7 = $this->getDataGenerator()->create_user(array('username'=>'abcjuja166-'.$currentyear));
        // абцхюя - abchjuja
        $user8 = $this->getDataGenerator()->create_user(array('username'=>'abchjuja166-'.$currentyear));
        // абвцхюя - abvchjuja
        $user9 = $this->getDataGenerator()->create_user(array('username'=>'abvchjuja166-'.$currentyear));
        // абцхчюя - abchchjuja
        $user10 = $this->getDataGenerator()->create_user(array('username'=>'abchchjuja166-'.$currentyear));
        // абвцхчюя - abvchchjuja
        $user11 = $this->getDataGenerator()->create_user(array('username'=>'abvchchjuja166-'.$currentyear));
        // абвгцхчюя - abvgchchjuja
        $user12 = $this->getDataGenerator()->create_user(array('username'=>'abvgchchjuja166-'.$currentyear));
        // абвгцхчшюя - abvgchchshjuja
        $user13 = $this->getDataGenerator()->create_user(array('username'=>'abvgchchshjuja166-'.$currentyear));
        // абвгцхчшщюя - abvgchchshshhjuja
        // абвгцхчшщьюя - abvgchchshshhjuja
        $username = $studentssyncplugin->generate_login('абвг', 'цхчшщь', 'юя', 'cohort-166');

        $this->assertEquals('abvgchchshshhjuja166-'.$currentyear, $username);
    }

    /**
     * Синхронизация студентов
     */
    public function test_sync() {
        $currentyear = date('Y');

        global $CFG, $DB;
        $this->resetAfterTest();

        $studentssyncplugin = enrol_get_plugin('studentssync');

        $trace = new null_progress_trace();
        $this->enable_plugin();
        $file = "$CFG->dirroot/enrol.txt";

        $studentrole = $DB->get_record('role', array('shortname'=>'student'));
        $this->assertNotEmpty($studentrole);

        $username1 = 'iii167-'.$currentyear;
        $user1 = $this->getDataGenerator()->create_user(array(
            'username' => $username1,
            'password' => md5($username1),
            'lastname' => 'Иванов',
            'firstname' => 'Иван',
            'middlename' => 'Иванович',
            'idnumber' => '20170001',
            'email' => $studentssyncplugin->generate_email($username1)
        ));

        // Установить путь к файлу
        $studentssyncplugin->set_config('location', $file);


        // Тест "Пользователь с таким идентификатором не существует. Добавление пользователя"
        $data ="lastname,firstname,patronymic,cohort,recordbook
            Васильев,Василий,Васильевич,ПРИН-167,20170002";

        file_put_contents($file, $data);

        $this->assertTrue(file_exists($file));
        $studentssyncplugin->sync($trace);

        // Удалим файл
        unlink($file);
        $this->assertFalse(file_exists($file));

        // Проверить данные в бд
        $username2 = 'vvv167-'.$currentyear;
        $this->assertTrue($DB->record_exists('user', array(
            'username' => $username2,
            'password' => md5($username2),
            'lastname' => 'Васильев',
            'firstname' => 'Василий',
            'middlename' => 'Васильевич',
            'idnumber' => '20170002',
            'email' => $studentssyncplugin->generate_email($username2)
        )));


        // Тест "Пользователь с таким идентификатором существует. Обновление пользователя"
        $data ="lastname,firstname,patronymic,cohort,recordbook
            Званов,Иван,Иванович,ПРИН-167,20170001";

        file_put_contents($file, $data);

        $this->assertTrue(file_exists($file));
        $studentssyncplugin->sync($trace);

        // Удалим файл
        unlink($file);
        $this->assertFalse(file_exists($file));

        // Проверить данные в бд
        $username2 = 'zii167-'.$currentyear;
        $this->assertTrue($DB->record_exists('user', array(
            'username' => $username2,
            'lastname' => 'Званов',
            'firstname' => 'Иван',
            'middlename' => 'Иванович',
            'idnumber' => '20170001',
        )));


        // Тест "Пользователь с таким логином существует. Добавление нового пользователя"
        $data ="lastname,firstname,patronymic,cohort,recordbook
            Васильев,Василий,Васильевич,ПРИН-167,20170003";

        file_put_contents($file, $data);

        $this->assertTrue(file_exists($file));
        $studentssyncplugin->sync($trace);

        // Удалим файл
        unlink($file);
        $this->assertFalse(file_exists($file));

        // Проверить данные в бд
        $username2 = 'vavv167-'.$currentyear;
        $this->assertTrue($DB->record_exists('user', array(
            'username' => $username2,
            'password' => md5($username2),
            'lastname' => 'Васильев',
            'firstname' => 'Василий',
            'middlename' => 'Васильевич',
            'idnumber' => '20170003',
            'email' => $studentssyncplugin->generate_email($username2)
        )));
    }
}
