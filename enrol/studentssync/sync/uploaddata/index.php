<?php
/**
 * Created by PhpStorm.
 * User: mypc
 * Date: 27.05.2017
 * Time: 15:31
 */

require('../../../../config.php');
require_once($CFG->libdir.'/adminlib.php');
require_once('lib.php');
require_once('data_form.php');

$page         = optional_param('page', 0, PARAM_INT);
$perpage      = optional_param('perpage', 5, PARAM_INT);        // сколько на странице

core_php_time_limit::raise(60*60); // 1 часа должно быть достаточно
raise_memory_limit(MEMORY_HUGE);

require_login();
admin_externalpage_setup('syncuploaddata');

$returnurl = new moodle_url('/enrol/studentssync/sync/uploaddata/index.php',
    array('perpage' => $perpage, 'page'=>$page));
$PAGE->set_url($returnurl);
$PAGE->set_pagelayout('admin');

// Форма загрузки данных с сайта ВолгГТУ
$mform = new admin_uploaddata_form();
$uploaddatasync = new upload_data_sync();

// Обработка нажатия кнопки "Загрузить данные"
if ($data = $mform->get_data()) {
    $uploaddatasync->sync();
}

// Форма синхронизации студентов из рейтинга на сайте ВолгГТУ
$mformsyncstudents = new admin_syncstudents_form();

// Обработка нажатия кнопки "Запустить синхронизацию студентов"
if ($data = $mformsyncstudents->get_data()) {
    $task = new enrol_studentssync\task\studentssync_sync_students_task();
    $task->execute();
}

// Считывание новых студентов
$arnewstudents = $uploaddatasync->read_students_from_db(array('isnew' => '1'));

echo $OUTPUT->header();
$mform->display();
$mformsyncstudents->display();
echo generate_html_table_for($arnewstudents, $page, $perpage);
$baseurl = new moodle_url('/enrol/studentssync/sync/uploaddata/index.php',
    array('perpage' => $perpage));
echo $OUTPUT->paging_bar(count($arnewstudents) - 1, $page, $perpage, $baseurl);
echo $OUTPUT->footer();

/**
 * Сгенерировать html-таблицу для массива студентов для указанной страницы с указанным периодом
 *
 * @param $arstudents - массив студентов
 * @param $page - страница
 * @param $perpage - период
 * @return string - html-таблица
 */
function generate_html_table_for($arstudents, $page, $perpage) {
    $table = new html_table();

    // Наименования столбцов
    $table->head = array();
    $table->head[] = "idnumber";
    $table->head[] = "lastname";
    $table->head[] = "firstname";
    $table->head[] = "middlename";
    $table->head[] = "group";
    $table->head[] = "email";

    $studentscount = 0;
    $rowscount = 0;

    // Выбрать из списка $page*$perpage, $perpage
    foreach($arstudents as $student) {
        // Строка
        if ($studentscount > $page * $perpage && $rowscount < $perpage) {
            $row = array();
            $row[] = $student["idnumber"];
            $row[] = $student["lastname"];
            $row[] = $student["firstname"];
            $row[] = $student["middlename"];
            $row[] = $student["group1"];
            $row[] = $student["email"];
            $table->data[] = $row;
            $rowscount++;
        }

        $studentscount++;
    }

    return html_writer::table($table);
}