<?php
/**
 * Created by PhpStorm.
 * User: mypc
 * Date: 27.05.2017
 * Time: 17:10
 */

defined('MOODLE_INTERNAL') || die();

global $CFG;

/**
 * Класс загрузки студентов и предметов с ВолгГТУ
 *
 * @author Sergey Kravchenko
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class upload_data_sync {

    /**
     * URL сайта ВолгГТУ, с которого необходимо получить список студентов
     *
     * @var string
     */
    private $url = 'http://www1.vstu.ru/studentu/reiting-studenta.html';

    /**
     * Путь к файлу со студентами
     *
     * @var string путь к файлу со студентами
     */
    private $studentsfilepath = "/enrol/studentssync/sync/uploaddata/students.txt";

    /**
     * Путь к файлу с предметами
     *
     * @var string путь к файлу с предметами
     */
    private $subjectsfilepath = "/enrol/studentssync/sync/uploaddata/subjects.txt";

    /**
     * Выполнить синхронизацию студентов и предметов с сайта ВолгГТУ
     */
    public function sync() {
        // Получить студентов текущего года.
        //$this->get_students_and_subjects_from_vstu($this->url);
        $arcuryearstudents = $this->get_students_from_vstu_for(0);

        // Получить предметы текущего года
        $arsubjects = $this->get_subjects_from_vstu_for(0);

        // Получить рейтинг студентов прошлого года
        $arprevyearstudents = $this->get_students_from_vstu_for(2, 1);

        // Определить студентов, восстановившихся из академического отпуска
        $aracademics = $this->identify_academics($arprevyearstudents, $arcuryearstudents);

        $archangedgroup = array();
        $arstudents = array();
        foreach ($arcuryearstudents as $number => $student) {
            $tmpstudent = $student;
            if (in_array($number, $aracademics)) {
                $tmpstudent['isacademician'] = 1;
            } else {
                $tmpstudent['isacademician'] = 0;
            }

            if (in_array($number, $archangedgroup)) {
                $tmpstudent['ischangedgroup'] = 1;
            } else {
                $tmpstudent['ischangedgroup'] = 0;
            }

            $arstudents[] = $tmpstudent;
        }

        $this->write_students_to_db($arstudents);
    }

    /**
     * Выполнить синхронизацию студентов из рейтинга на сайте ВолгГТУ
     */
    public function sync_students() {
        core_php_time_limit::raise();
        raise_memory_limit(MEMORY_HUGE);

        // Получить студентов текущего семестра
        $arcursemesterstudents = $this->get_students_from_vstu_for(0);

        // Получить всех студентов предыдущего семестра
        $arprevsemesterstudents = $this->get_students_from_vstu_for(1, 0, 0);

        // Получить рейтинг всех студентов прошлого года
        $arprevyearstudents = $this->get_students_from_vstu_for(2, 1, 0);

        // Определить студентов, восстановившихся из академического отпуска
        $aracademics = $this->identify_academics($arprevyearstudents, $arcursemesterstudents);

        // Определить студентов, осуществивших перевод из одной группы в другую
        $archangedgroup = $this->identify_changed_group($arprevsemesterstudents, $arcursemesterstudents);

        $arstudents = array();
        $aracademics = array();
        $archangedgroup = array();
        foreach ($arcursemesterstudents as $number => $student) {
            $tmpstudent = $student;
            $tmpstudent['isacademician'] = in_array($number, $aracademics) ? 1 : 0;
            $tmpstudent['ischangedgroup'] = in_array($number, $archangedgroup) ? 1 : 0;
            $arstudents[$number] = $tmpstudent;
        }

        // Записать студентов в базу данных
        $this->write_students_to_db($arstudents);
    }

    /**
     * Выполнить инициализацию сеанса cURL
     *
     * @param $url - url сайта
     * @return resource - дескриптор для работы с cURL
     */
    private function init_curl($url) {
        $ch = curl_init();

        // Устанавливаем параметры.
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);

        return $ch;
    }

    /**
     * Получить список студентов и предметов с vstu.ru
     *
     * @param $url - url сайта
     * @return int - код ошибки
     */
    private function get_students_and_subjects_from_vstu($url) {
        global $CFG;

        $ch = $this->init_curl($url);

        // Выполняем запрос.
        $htmlcontent = curl_exec($ch);
        if ($htmlcontent === false) {
            return -1;
        }

        // Устанавливаем сеанс сURL как POST-сеанс.
        curl_setopt($ch, CURLOPT_POST, 1);

        // Выбираем семестр на странице.
        $htmlcontent = $this->get_content_page_vstu($ch,
            "rating[semester]=28&rating[branch]=1&form_build_id=".
            $this->get_form_build_id_value_from($htmlcontent),
            "<select name=\"rating[branch]\"");
        if ($htmlcontent == -1) {
            return -1;
        }

        // Выбираем филиал на странице.
        $htmlcontent = $this->get_content_page_vstu($ch,
            "rating[semester]=28&rating[branch]=1&form_build_id=".
            $this->get_form_build_id_value_from($htmlcontent),
            "<select name=\"rating[faculty]\"");
        if ($htmlcontent == -1) {
            return -1;
        }

        // Выбираем факультет на странице.
        $htmlcontent = $this->get_content_page_vstu($ch,
            "rating[branch]=1&rating[faculty]=107&form_build_id=".
            $this->get_form_build_id_value_from($htmlcontent),
            "<select name=\"rating[group]\"");
        if ($htmlcontent == -1) {
            return -1;
        }

        // Формируем массив групп.
        $res = preg_match_all("#<select name=\"rating\[group\]\" ".
            "class=\"form-select\" ".
            "id=\"edit-rating-group\" >(.+?)</select>#is",
            $htmlcontent, $arr);
        if (!$res) {
            return -1;
        }

        preg_match_all("#value=\"(.+?)\"#is", $arr[1][0], $arr);
        $argroups = $arr[1];

        // Сформировать список студентов и предметов.
        $arstudents = array();
        $arsubjects = array();
        foreach ($argroups as $group) {
            if ($group) {
                // Выбрать студентов заданной группы на странице.
                if ($htmlcontent != -1) {
                    $htmlcontent = $this->get_content_page_vstu($ch,
                        "rating[group]=".$group."&rating[student]=0&form_build_id=".
                        $this->get_form_build_id_value_from($htmlcontent),
                        "<select name=\"rating[student]\"");
                }

                // Добавить студентов группы в общий список.
                if ($htmlcontent != -1) {
                    $arstudents = array_merge($arstudents, $this->get_students_for_group_from($htmlcontent, $group));
                }

                // Выбрать предметы заданной группы на странице.
                if ($htmlcontent != -1) {
                    $htmlcontent = $this->get_content_page_vstu($ch,
                        "rating[group]=".$group."&rating[whole_group]=1&rating[student]=0&form_build_id=".
                        $this->get_form_build_id_value_from($htmlcontent),
                        "<ul class=\"rating-legend\"");
                }

                // Добавить предметы группы в общий список.
                if ($htmlcontent != -1) {
                    $arsubjects = array_merge($arsubjects, $this->get_subjects_for_group_from($htmlcontent, $group));
                }
            }
        }

        // Сформировать txt-файл со списком студентов.
        $this->generate_txtfile_with_students($CFG->dirroot . $this->studentsfilepath, $arstudents);

        // Сформировать txt-файл со списком предметов.
        $this->generate_txtfile_with_subjects($CFG->dirroot . $this->subjectsfilepath, $arsubjects);
    }

    /**
     * Получить студентов указанного семестра из рейтинга на сайте ВолгГТУ
     *
     * @param $semester - позиция семестра в select
     * @param $rating - получать рейтинг или нет, по умолчанию 0
     * @param $filter - применять фильтр к факультетам и группам или нет, по умолчанию 0
     * @return array|int массив студентов или -1 в случае ошибки
     */
    public function get_students_from_vstu_for($semesterpos, $rating = 0, $filter = 1) {
        if ($semesterpos < 0 || $semesterpos > 3) {
            return -1;
        }

        global $CFG;
        $url = $this->url;                                          // URL сайта ВолгГТУ
        $ch = $this->init_curl($url);                               // Дескриптор сеанса cURL
        $vstuid = 1;                                                // Идентификатор ВолгГТУ
        $studentssyncplugin = enrol_get_plugin('studentssync');     // Плагин "Запись студентов ВолгГТУ на предметы"

        // Выполняем запрос.
        $htmlcontent = curl_exec($ch);
        if ($htmlcontent === false) {
            return -1;
        }

        curl_setopt($ch, CURLOPT_POST, 1);                  // Устанавливаем сеанс сURL как POST-сеанс.

        // Получаем идентификатор семестра
        $arsemesters = $this->get_semesters_on_page($htmlcontent);
        if ($arsemesters == -1) {
            return -1;
        }

        $arkeyssemesters = array_keys($arsemesters);
        $semesterid = $arkeyssemesters[$semesterpos];

        // Получаем html-содержимое страницы для указанного семестра
        $htmlcontent = $this->get_semester_page($ch, $htmlcontent, $semesterid);
        if ($htmlcontent == -1) {
            return -1;
        }

        // Получаем html-содержимое страницы для указанного филиала
        $htmlcontent = $this->get_branch_page($ch, $htmlcontent, $semesterid, $vstuid);
        if ($htmlcontent == -1) {
            return -1;
        }

        // Определяем факультеты на странице
        $arfaculties = $this->get_faculties_on_page($htmlcontent);

        // Получаем факультеты для обработки из settings.php
        if ($filter) {
            $arconfigfaculties = get_config('enrol_studentssync', 'show_faculties');
            $arfacultiesids = explode(',', str_replace('show_faculties_', '', $arconfigfaculties));
        } else {
            $arfacultiesids = array('112', '113', '114', '105', '111', '103', '108', '106', '107', '102');
        }

        // Выбираем студентов из групп заданных факультетов на странице
        $arstudents = array();
        foreach($arfaculties as $facultyid => $facultyname) {
            if (in_array($facultyid, $arfacultiesids)) {
                $htmlcontentgroup = $this->get_faculty_page($ch, $htmlcontent, $vstuid, $facultyid);
                if ($htmlcontentgroup == -1) {
                    return -1;
                }

                $argroups = $this->get_groups_on_page($htmlcontentgroup);

                // Выбираем группы
                if ($filter) {
                    $arconfiggroups = get_config('enrol_studentssync', 'show_groups');
                    $arfilter = explode(',', $arconfiggroups);
                    $insensitivity = get_config('enrol_studentssync', 'insensitivity_show_groups');
                    $insensitivitychar = $insensitivity ? 'i' : '';
                } else {
                    $arconfiggroups = "";
                }

                $arfiltergroups = array();
                foreach ($argroups as $group) {
                    if (!empty(trim($arconfiggroups))) {
                        foreach ($arfilter as $filter) {
                            if (preg_match('#' . $filter . '#' . $insensitivitychar . 'u', $group)) {
                                $arfiltergroups[$group] = $group;
                                break;
                            }
                        }
                    } else {
                        $arfiltergroups[$group] = $group;
                    }
                }

                // Получаем студентов для групп
                foreach ($arfiltergroups as $group) {
                    $artmpstudents = array();
                    $htmlcontentstudents = $this->get_group_page($ch, $htmlcontentgroup, $group);
                    if ($htmlcontentstudents == -1) {
                        return -1;
                    }

                    $artmpstudents = $this->get_students_for_group_from($htmlcontentstudents, $group);

                    // Получаем рейтинг студентов
                    if ($rating) {
                        foreach ($artmpstudents as $arstudent) {
                            $htmlcontentrating = $this->get_student_rating_page($ch, $htmlcontentstudents,
                                $group, $arstudent['number']);
                            if ($htmlcontentrating != -1) {
                                $rating = $this->get_rating_for_student_from($htmlcontentrating);
                                if ($rating == -1) {
                                    return -1;
                                }

                                $arstudent['rating'] = $rating;
                            } else {
                                $arstudent['rating'] = array();
                            }

                            $arstudents[$arstudent['number']] = $arstudent;
                        }
                    } else {
                        $arstudents = $arstudents + $artmpstudents;
                    }
                }
            }
        }

        return $arstudents;
    }

    /**
     * Получить предметы указанного семестра из рейтинга на сайте ВолгГТУ
     *
     * @param $semester - позиция семестра в select
     * @return array|int массив предметов или -1 в случае ошибки
     */
    public function get_subjects_from_vstu_for($semesterpos) {
        if ($semesterpos < 0 || $semesterpos > 3) {
            return -1;
        }

        global $CFG;
        $url = $this->url;                                          // URL сайта ВолгГТУ
        $ch = $this->init_curl($url);                               // Дескриптор сеанса cURL
        $vstuid = 1;                                                // Идентификатор ВолгГТУ
        $studentssyncplugin = enrol_get_plugin('studentssync');     // Плагин "Запись студентов ВолгГТУ на предметы"

        // Выполняем запрос.
        $htmlcontent = curl_exec($ch);
        if ($htmlcontent === false) {
            return -1;
        }

        curl_setopt($ch, CURLOPT_POST, 1);                  // Устанавливаем сеанс сURL как POST-сеанс.

        // Получаем идентификатор семестра
        $arsemesters = $this->get_semesters_on_page($htmlcontent);
        if ($arsemesters == -1) {
            return -1;
        }

        $arkeyssemesters = array_keys($arsemesters);
        $semesterid = $arkeyssemesters[$semesterpos];

        // Получаем html-содержимое страницы для указанного семестра
        $htmlcontent = $this->get_semester_page($ch, $htmlcontent, $semesterid);
        if ($htmlcontent == -1) {
            return -1;
        }

        // Получаем html-содержимое страницы для указанного филиала
        $htmlcontent = $this->get_branch_page($ch, $htmlcontent, $semesterid, $vstuid);
        if ($htmlcontent == -1) {
            return -1;
        }

        // Определяем факультеты на странице
        $arfaculties = $this->get_faculties_on_page($htmlcontent);

        // Получаем факультеты для обработки из settings.php
        //$arconfigfaculties = get_config('enrol_studentssync', 'show_faculties');
        $arconfigfaculties = 'show_faculties_107'; // Только ФЭВТ
        $arfacultiesids = array(str_replace('show_faculties_', '', $arconfigfaculties));

        // Выбираем предметы для групп заданных факультетов на странице
        $arsubjects = array();
        foreach($arfaculties as $facultyid => $facultyname) {
            if (in_array($facultyid, $arfacultiesids)) {
                $htmlcontentgroup = $this->get_faculty_page($ch, $htmlcontent, $vstuid, $facultyid);
                if ($htmlcontentgroup == -1) {
                    return -1;
                }

                $argroups = $this->get_groups_on_page($htmlcontentgroup);

                // Выбираем группы
                $filter = 'прин-46[6-7]';
                $arfilter = explode(',', $filter);
                $arfiltergroups = array();
                foreach ($argroups as $group) {
                    foreach ($arfilter as $filter) {
                        if (preg_match('#' . $filter . '#iu', $group)) {
                            $arfiltergroups[$group] = $group;
                            break;
                        }
                    }
                }

                $htmlcontent = $htmlcontentgroup;

                // Получаем предметы групп
                foreach ($arfiltergroups as $group) {
                    $htmlcontentgroup = $this->get_group_page($ch, $htmlcontent, $group);
                    if ($htmlcontentgroup == -1) {
                        return -1;
                    }

                    $htmlcontentrating = $this->get_rating_page($ch, $htmlcontentgroup, $group);
                    if ($htmlcontentrating == -1) {
                        return -1;
                    }

                    $artmpsubjects = $this->get_subjects_for_group_from($htmlcontentrating, $group);
                    $arsubjects = array_merge($arsubjects, $artmpsubjects);
                }
            }
        }

        return $arsubjects;
    }

    /**
     * Определить идентификатор текущего семестра на странице
     *
     * @param $htmlcontent - html-содержимое страницы
     * @return int - -1 в случае ошибки или идентификатор текущего семестра
     */
    private function get_semesters_on_page($htmlcontent) {
        $arsemesters = $this->get_options_values_for('rating\[semester\]', $htmlcontent);
        if ($arsemesters == -1 || empty($arsemesters)) {
            return -1;
        }

        return $arsemesters;
    }

    /**
     * Получить список факультетов на странице
     *
     * @param $htmlcontent - html-содержимое страницы
     * @return int - -1 в случае ошибки или список факультетов
     */
    private function get_faculties_on_page($htmlcontent) {
        $arfaculties = $this->get_options_values_for('rating\[faculty\]', $htmlcontent);
        if ($arfaculties == -1 || empty($arfaculties)) {
            return -1;
        }

        return $arfaculties;
    }

    /**
     * Получить список групп студентов на странице
     *
     * @param $htmlcontent - html-содержимое страницы
     * @return int - -1 в случае ошибки или список групп студентов
     */
    private function get_groups_on_page($htmlcontent) {
        $argroups = $this->get_options_values_for('rating\[group\]', $htmlcontent);
        if ($argroups == -1 || empty($argroups)) {
            return -1;
        }

        return $argroups;
    }

    /**
     * Получить html-содержимое страницы для указанного семестра
     *
     * @param $ch - сессия cURL
     * @param $htmlcontent - html-содержимое страницы
     * @param $semesterid - идентификатор семестра
     * @return int|mixed - -1 в случае ошибки или html-содержимое страницы
     */
    private function get_semester_page($ch, $htmlcontent, $semesterid) {
        // Выбираем семестр на странице.
        $htmlcontent = $this->get_content_page_vstu($ch,
            "rating[semester]=". $semesterid . "&rating[branch]=1&form_build_id=".
            $this->get_form_build_id_value_from($htmlcontent),
            "<select name=\"rating[branch]\"");
        if ($htmlcontent == -1) {
            return -1;
        }

        return $htmlcontent;
    }

    /**
     * Получить html-содержимое страницы для указанного филиала
     *
     * @param $ch - сессия cURL
     * @param $htmlcontent - html-содержимое страницы
     * @param $semesterid - идентификатор семестра
     * @param $branchid - идентификатор филиала
     * @return int|mixed - -1 в случае ошибки или html-содержимое страницы
     */
    private function get_branch_page($ch, $htmlcontent, $semestrid, $branchid) {
        $htmlcontent = $this->get_content_page_vstu($ch,
            "rating[semester]=". $semestrid . "&rating[branch]=" . $branchid . "&form_build_id=".
            $this->get_form_build_id_value_from($htmlcontent),
            "<select name=\"rating[faculty]\"");
        if ($htmlcontent == -1) {
            return -1;
        }

        return $htmlcontent;
    }

    /**
     * Получить html-содержимое страницы для указанного факультета
     *
     * @param $ch - сессия cURL
     * @param $htmlcontent - html-содержимое страницы
     * @param $branchid - идентификатор филиала
     * @param $facultyid - идентификатор факультета
     * @return int|mixed - -1 в случае ошибки или html-содержимое страницы
     */
    private function get_faculty_page($ch, $htmlcontent, $branchid, $facultyid) {
        $htmlcontent = $this->get_content_page_vstu($ch,
            "rating[branch]=" . $branchid . "&rating[faculty]=" . $facultyid . "&form_build_id=".
            $this->get_form_build_id_value_from($htmlcontent),
            "<select name=\"rating[group]\"");
        if ($htmlcontent == -1) {
            return -1;
        }

        return $htmlcontent;
    }

    /**
     * Получить html-содержимое страницы для указанной группы
     *
     * @param $ch - сессия cURL
     * @param $htmlcontent - html-содержимое страницы
     * @param $group - имя группы
     * @return int|mixed - -1 в случае ошибки или html-содержимое страницы
     */
    private function get_group_page($ch, $htmlcontent, $group) {
        $htmlcontent = $this->get_content_page_vstu($ch,
            "rating[group]=" . $group . "&rating[students]=0&form_build_id=".
            $this->get_form_build_id_value_from($htmlcontent),
            "<select name=\"rating[student]\"");
        if ($htmlcontent == -1) {
            return -1;
        }

        return $htmlcontent;
    }

    /**
     * Получить html-содержимое страницы с рейтингом для указанной группы
     *
     * @param $ch - сессия cURL
     * @param $htmlcontent - html-содержимое страницы с рейтингм
     * @param $group - имя группы
     * @return int|mixed - -1 в случае ошибки или html-содержимое страницы с рейтингом
     */
    private function get_rating_page($ch, $htmlcontent, $group) {
        $htmlcontent = $this->get_content_page_vstu($ch,
            "rating[group]=" . $group . "&rating[whole_group]=1&rating[student]=0&form_build_id=".
            $this->get_form_build_id_value_from($htmlcontent),
            "<ul class=\"rating-legend\"");
        if ($htmlcontent == -1) {
            return -1;
        }

        return $htmlcontent;
    }

    /**
     * Получить html-содержимое страницы с рейтингом для указанного студента
     *
     * @param $ch - сессия cURL
     * @param $htmlcontent - html-содержимое страницы с рейтингом для указанного студента
     * @param $group - имя группы
     * @param $idnumber - идентификационный номер студента
     * @return int|mixed - -1 в случае ошибки или html-содержимое страницы с рейтингом для указанного студента
     */
    private function get_student_rating_page($ch, $htmlcontent, $group, $idnumber) {
        $htmlcontent = $this->get_content_page_vstu($ch,
            "rating[group]=" . $group . "&rating[student]=" . $idnumber .
            "&form_build_id=" .
            $this->get_form_build_id_value_from($htmlcontent),
            "<div id=\"rating-data\">");
        if ($htmlcontent == -1) {
            return -1;
        }

        return $htmlcontent;
    }

    /**
     * Получить содержимое html-страницы vstu.ru при переданных POST-параметрах
     *
     * @param $curlhandle - curlhandle
     * @param $postparamsstr - строка с POST-параметрами
     * @param $verificationstr - строка верификации ожидаемой страницы
     * @return int|mixed - html-содержимое указанной страницы, либо -1 в случае ошибки
     */
    private function get_content_page_vstu($curlhandle, $postparamsstr, $verificationstr) {
        curl_setopt($curlhandle, CURLOPT_POSTFIELDS, $postparamsstr.
            "&form_id=rating_student_form");

        // Выполняем запрос.
        $htmlcontent = curl_exec($curlhandle);
        if ($htmlcontent === false) {
            return -1;
        }

        if (!strstr($htmlcontent, $verificationstr)) {
            return -1;
        }

        return $htmlcontent;
    }

    /**
     * Получить значение hidden-поля "form_build_id" на странице
     *
     * @param $htmlcontent - содержимое html-страницы
     * @return mixed - значение hidden-поля "form_build_id", либо ничего
     */
    private function get_form_build_id_value_from($htmlcontent) {
        preg_match_all('#<input type="hidden" name="form_build_id" (.+?)  />#is', $htmlcontent, $arr);
        preg_match_all('#value="(.+?)"#', $arr[0][1], $arr);
        return $arr[1][0];
    }

    /**
     * Получить значения option для указанного select на странице
     *
     * @param $select - имя селекта
     * @param $htmlcontent - содержимое html-страницы
     * @return array|int - массив значений или -1 в случае ошибки
     */
    private function get_options_values_for($select, $htmlcontent) {
        preg_match_all('#<select name="' . $select . '"(.+?)</select>#is', $htmlcontent, $arr);
        if (empty($arr[1])) {
            return -1;
        }

        preg_match_all('#<option value=(.+?)</option>#', $arr[1][0], $arr);
        if (empty($arr[0])) {
            return -1;
        }

        $arvalues = array();
        foreach ($arr[0] as $optionline) {
            preg_match_all('#value="((?!0)(.+?))">#', $optionline, $key);
            preg_match_all('#">((?!0)(.+?))<#', $optionline, $value);
            if (!(empty($key[1]) || empty($value[1]))) {
                $arvalues[$key[1][0]] = $value[1][0];
            }
        }

        if (empty($arvalues)) {
            return -1;
        }

        return $arvalues;
    }

    /**
     * Получить студентов группы из html-содержимого
     *
     * @param $htmlcontent - html-содержимое
     * @param $group - группа
     * @return array - массив студентов
     */
    private function get_students_for_group_from($htmlcontent, $group) {
        $arstudents = array();

        // Получаем select из html-содержимого.
        $res = preg_match_all("#<select name=\"rating\[student\]\"".
            " class=\"form-select\"".
            " id=\"edit-rating-student\"".
            " >(.+?)</select>#is", $htmlcontent, $arr);

        if ($res) {
            // Вырезаем option из select.
            preg_match_all("#<option (.+?)/option>#is", $arr[1][0], $arr);

            $student = array();
            foreach ($arr[1] as $item) {
                // Вырезаем фамилию, которая заключена внутри тега option.
                preg_match_all("#\">(.+?)<#is", $item, $arstudentfio);

                // Вырезаем номер студента, который находится в value тега option.
                preg_match_all("#value=\"(.+?)\"#is", $item, $arstudentnumber);

                if ((isset($arstudentfio[1][0]) && isset($arstudentnumber[1][0]))
                    && ($arstudentfio[1][0] != '-' || $arstudentnumber[1][0])
                ) {
                    $arformattedfio = $this->separate_student_fio($arstudentfio[1][0]);

                    $student['lastname'] = $arformattedfio['lastname'];
                    $student['firstname'] = $arformattedfio['firstname'];
                    $student['middlename'] = $arformattedfio['middlename'];
                    $student['number'] = $arstudentnumber[1][0];
                    $student['group1'] = $group;

                    $arstudents[$arstudentnumber[1][0]] = $student;
                }
            }
        }

        return $arstudents;
    }

    /**
     * Получить предметы группы из html-содержимого
     *
     * @param $htmlcontent - html-содержимое
     * @param $group - группа
     * @return array - массив предметов
     */
    private function get_subjects_for_group_from($htmlcontent, $group) {
        $arsubjects = array();

        // Получаем ul из html-содержимого.
        $res = preg_match_all("#<ul class=\"rating-legend\">(.+?)</ul>#is", $htmlcontent, $arr);

        if ($res) {
            // Вырезаем li из ul.
            preg_match_all("#<li>(.+?)</li>#is", $arr[1][0], $arr);

            $subject = array();
            foreach ($arr[1] as $item) {
                // Вырезаем предмет из тега li.
                preg_match_all("#— (.+?);#is", $item, $arsubject);

                if (isset($arsubject[1][0])) {
                    $subject['subject'] = $arsubject[1][0];
                    $subject['group'] = $group;

                    $arsubjects[] = $subject;
                }
            }
        }

        return $arsubjects;
    }

    /**
     * Получить рейтинг студента из html-содержимого
     *
     * @param $htmlcontent - html-содержимое
     * @return array|int - массив предметов и оценок или -1 в случае ошибки
     */
    private function get_rating_for_student_from($htmlcontent) {
        $arratings = array();

        // Получаем таблицу с рейтингом из html-содержимого.
        $res = preg_match_all("#<div id=\"rating-data\">(.+?)</div>#is", $htmlcontent, $arr);
        if (!$res) {
            return -1;
        }

        // Получаем tbody указанной таблицы
        $res = preg_match_all("#<tbody>(.+?)</tbody>#is", $arr[0][0], $arr);
        if (!$res) {
            return -1;
        }

        // Получаем tr указанной таблицы
        $res = preg_match_all("#<tr>(.+?)</tr>#is", $arr[0][0], $artrs);
        if (!$res) {
            return -1;
        }

        foreach ($artrs[0] as $tr) {
            $tr = str_replace("<tr>", "", $tr);
            $tr = str_replace("</tr>", "", $tr);
            $tr = str_replace("<td style=\"text-align: center\">", "", $tr);
            $tr = str_replace("</td>", "", $tr);
            $tr = str_replace("<td>", "", $tr);
            $artmptds = explode("\n", $tr);
            $artds = array();
            foreach ($artmptds as $td) {
                $tmptd = trim($td);
                if ($tmptd !== "") {
                    $artds[] = $tmptd;
                }
            }

            $count = count($artds);
            $arratings[trim($artds[0])] = trim($artds[$count - 1]);
        }

        return $arratings;
    }

    /**
     * Разделить ФИО студента на составляющие - фамилия, имя, отчество
     *
     * @param $fio - строка с ФИО
     * @return array - массив с составляющими - фамилия, имя, отчество
     */
    public function separate_student_fio($fio) {
        $arfio = explode(" ", preg_replace ('#(я|Я)(я|Я)\*#', '', $fio));
        $pos = 0;
        $arformattedfio = array(
            'lastname' => '',
            'firstname' => '',
            'middlename' => ''
        );

        foreach ($arfio as $partfio) {
            if ($pos == 0) {
                // Добавить фамилию.
                // Если фамилия является двойной, напр. "ПЕТРОВ-ИВАНОВ".
                if (strstr($partfio, '-')) {
                    $arlastname = explode("-", $partfio);

                    $arnewlastname = array();
                    foreach ($arlastname as $partlastname) {
                        $partlastname = mb_strtolower($partlastname);
                        $arnewlastname[] = $this->capitalize_first_letter($partlastname);
                    }

                    $arformattedfio['lastname'] = implode("-", $arnewlastname);
                } else if (mb_strtoupper($partfio) === $partfio) { // Если фамилия в верхнем регистре.
                    $partfio = mb_strtolower($partfio);
                    $arformattedfio['lastname'] = $this->capitalize_first_letter($partfio);
                } else {
                    $arformattedfio['lastname'] = $partfio;
                }
            } else if ($pos == 1) {
                // Если второе слово в верхнем регистре.
                if (mb_strtoupper($partfio) === $partfio) {
                    $partfio = mb_strtolower($partfio);

                    // Если вся строка находится в верхнем регистре.
                    if (mb_strtoupper($fio) === $fio) {
                        // Второе слово является именем
                        $arformattedfio['firstname'] = $this->capitalize_first_letter($partfio);
                    } else {
                        // Добавляем разделитель для фамилии.
                        $arformattedfio['lastname'] .= ' ';

                        // Второе слово является частью фамилии.
                        $arformattedfio['lastname'] .= $this->capitalize_first_letter($partfio);
                    }
                } else {
                    // Второе слово является именем.
                    $arformattedfio['firstname'] = $this->capitalize_first_letter($partfio);
                }
            } else {
                // Добавляем имя
                if ($arformattedfio['firstname'] === '') { // Если имя непустое.
                    $partfio = mb_strtolower($partfio);
                    $arformattedfio['firstname'] .= $this->capitalize_first_letter($partfio);
                } else {
                    // Добавляем разделитель для отчества.
                    if ($arformattedfio['middlename'] !== '' && $pos > 2) {
                        $arformattedfio['middlename'] .= ' ';
                    }

                    // Добавляем отчество.
                    if (preg_replace('#-+#', '', $partfio) !== '' && preg_replace('#=+#', '', $partfio) !== '') {
                        if (mb_strtoupper($partfio) === $partfio) {
                            $partfio = mb_strtolower($partfio);
                            $arformattedfio['middlename'] .= $this->capitalize_first_letter($partfio);
                        } else {
                            $arformattedfio['middlename'] .= $partfio;
                        }
                    }
                }
            }

            $pos++;
        }

        return $arformattedfio;
    }

    /**
     * Сделать первую букву строки заглавной
     *
     * @param $str - строка
     * @return string - строка после форматирования
     */
    private function capitalize_first_letter($str) {
        return mb_strtoupper(mb_substr($str, 0, 1)) . mb_substr($str, 1);
    }

    /**
     * Сгенерировать txt-файл со списком студентов
     *
     * @param $filename - путь к файлу
     * @param $arstudents - массив студентов
     * @param string $delimiter - раделитель, по умолчанию - ','
     */
    private function generate_txtfile_with_students($filename, $arstudents, $delimiter = ',') {
        $text = "lastname".$delimiter.
            "firstname".$delimiter.
            "middlename".$delimiter.
            "email".$delimiter.
            "cohort".$delimiter.
            "idnumber\n";

        // Сформировать содержимое txt-файла
        foreach ($arstudents as $student) {
            $text .= $student['lastname'].$delimiter;
            $text .= $student['firstname'].$delimiter;
            $text .= $student['middlename'].$delimiter;
            $text .= $this->generate_email('std' . $student['number']).$delimiter;
            $text .= $student['group'].$delimiter;
            $text .= $student['number']."\n";
        }

        // Записать в файл
        $this->write_to_file($filename, $text);
    }

    /**
     * Сгенерировать txt-файл со списком предметов
     *
     * @param $filename - путь к файлу
     * @param $arsubjects - массив предметов
     * @param string $delimiter - раделитель, по умолчанию - ','
     */
    private function generate_txtfile_with_subjects($filename, $arsubjects, $delimiter = ',') {
        $text = "cohort".$delimiter."course\n";

        // Сформировать содержимое txt-файла
        foreach ($arsubjects as $subject) {
            $text .= $subject['group'].$delimiter;
            $text .= $subject['subject']."\n";
        }

        // Записать в файл
        $this->write_to_file($filename, $text);
    }

    /**
     * Записать в файл указанный текст
     *
     * @param $filename - имя файла
     * @param $text - текст
     */
    private function write_to_file($filename, $text) {
        // Открыть текстовый файл.
        $f = fopen($filename, "w");

        // Записать строку текста.
        fwrite($f, $text);

        // Закрыть текстовый файл.
        fclose($f);
    }

    /**
     * Генерирует email по заданной строке
     */
    private function generate_email($str) {
        $suffix = 'moodle.com';
        return $str . '@' . $suffix;
    }

    /**
     * Идентифицирует студентов, вернувшихся из академического отпуска
     *
     * @param $arlastyearratings - рейтинги студентов за прошлый год
     * @param $arcuryearstudents - студенты текущего года
     * @return array - студенты, вервушиеся из академического отпуска
     */
    public function identify_academics($arlastyearratings, $arcuryearstudents) {
        $aracademics = array();

        foreach ($arcuryearstudents as $idnumber => $student) {
            if (array_key_exists($idnumber, $arlastyearratings)) {
                $arrating = $arlastyearratings[$idnumber]["rating"];

                foreach ($arrating as $mark) {
                    if ($mark == "не допущен") {
                        $aracademics[] = $idnumber;
                        break;
                    }
                }
            }
        }

        return $aracademics;
    }

    /**
     * Записать студентов в базу данных
     *
     * @param $arstudents - массив студентов
     */
    public function write_students_to_db($arstudents) {
        global $DB;
        $table = 'enrol_studentssync_users';

        // Получить всех студентов из базы данных
        $arstudentsdb = $DB->get_records($table, null, '', 'idnumber, id');

        // Определить студентов, которые уже есть в базе данных и которых еще нет
        $arstudentsadd = array();
        $arstudentsupd = array();
        foreach ($arstudents as $idnumber => $student) {
            if(key_exists($idnumber, $arstudentsdb)) {
                $arstudentsupd[] = $student;
            } else {
                $arstudentsadd[] = $student;
            }
        }

        // Добавить новых студентов
        if (!empty($arstudentsadd)) {
            $arstdobjects = array();
            foreach($arstudentsadd as $student) {
                $stdobject = new stdClass();
                $stdobject->lastname = $student['lastname'];
                $stdobject->firstname = $student['firstname'];
                $stdobject->middlename = $student['middlename'];
                $stdobject->group1 = $student['group1'];
                $stdobject->idnumber = $student['number'];
                $stdobject->email = "std" . $student['number'] . "@moodle.com";
                $stdobject->isnew = '1';
                $stdobject->isacademician = $student['isacademician'];
                $stdobject->ischangedgroup = $student['ischangedgroup'];
                $stdobject->creationtime = time();
                $stdobject->changedtime = time();
                $arstdobjects[] = $stdobject;
            }

            if (!empty($arstdobjects)) {
                $DB->insert_records($table, $arstdobjects);
            }
        }

        // Обновить студентов
        if (!empty($arstudentsupd)) {
            foreach ($arstudentsupd as $student) {
                $stdobject = new stdClass();
                $stdobject->id = $arstudentsdb[$student['number']]->id;
                $stdobject->lastname = $student['lastname'];
                $stdobject->firstname = $student['firstname'];
                $stdobject->middlename = $student['middlename'];
                $stdobject->group1 = $student['group1'];
                $stdobject->isnew = '0';
                $stdobject->isacademician = $student['isacademician'];
                $stdobject->ischangedgroup = $student['ischangedgroup'];
                $stdobject->changedtime = time();
                $DB->update_record($table, $stdobject);
            }
        }
    }

    /**
     * Считать студентов из базы данных
     *
     * @param array $conditions - опциональный массив $fieldname => requestedvalue с AND между ними
     * @return array - массив студентов
     */
    public function read_students_from_db(array $conditions = null) {
        global $DB;
        $table = 'enrol_studentssync_users';
        $arstudents = array();

        $arstudentsdb = $DB->get_records($table, $conditions, '', '*');
        $arstudents = convert_to_array($arstudentsdb);

        return $arstudents;
    }

    /**
     * Идентифицирует студентов, осуществивших перевод из одной группы в другую
     *
     * @param $arprevsemesterstudents - студенты прошлого семестра
     * @param $arcursemesterstudents - студенты текущего семестра
     * @return array - студенты, осуществившие перевод из одной группы в другую
     */
    public function identify_changed_group($arprevsemesterstudents, $arcursemesterstudents) {
        $archangedgroup = array();

        foreach ($arcursemesterstudents as $idnumber => $student) {
            if (array_key_exists($idnumber, $arprevsemesterstudents)) {
                $prevgroup = $arprevsemesterstudents[$idnumber]["group1"];
                $a = mb_strpos($prevgroup, "-");
                $prevprefixedpart = mb_strtoupper(mb_substr($prevgroup, 0, mb_strpos($prevgroup, "-")));
                $curgroup = $student["group1"];
                $curprefixedpart = mb_strtoupper(mb_substr($curgroup, 0, mb_strpos($curgroup, "-")));
                if ($prevprefixedpart != $curprefixedpart) {
                    $archangedgroup[] = $idnumber;
                }
            }
        }

        return $archangedgroup;
    }
}