<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Тесты для класса загрузки студентов и предметов с ВолгГТУ
 *
 * @package    enrol_studentssync
 * @category   phpunit
 * @copyright  2017, Sergey Kravchenko <kravchenko.sgv@gmail.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

global $CFG;

require_once($CFG->dirroot . '/enrol/studentssync/sync/uploaddata/lib.php');


class upload_data_sync_lib_testcase extends advanced_testcase {

    /**
     * Разделить ФИО студента на составляющие - фамилия, имя, отчество
     */
    public function test_separate_student_fio() {
        $uploaddatasync = new upload_data_sync();

        // ФИО в верхнем регистре.
        $arformattedfio = $uploaddatasync->separate_student_fio(
            "ПИВОВАРОВ ВАЛЕНТИН ВАСИЛЬЕВИЧ");
        $arexpectedfio = array(
            'lastname' => 'Пивоваров',
            'firstname' => 'Валентин',
            'middlename' => 'Васильевич'
        );
        $this->assertEquals($arformattedfio, $arexpectedfio);

        $arformattedfio = $uploaddatasync->separate_student_fio(
            "ДУ РОЗАРИУ АЛВЕШ КАРДОЗУ ДАНИЛУ ВАЛДОМАР");
        $arexpectedfio = array(
            'lastname' => 'Ду',
            'firstname' => 'Розариу',
            'middlename' => 'Алвеш Кардозу Данилу Валдомар'
        );
        $this->assertEquals($arformattedfio, $arexpectedfio);

        // Фамилия в верхнем регистре.
        $arformattedfio = $uploaddatasync->separate_student_fio(
            "ЛИ Дмитрий Олегович");
        $arexpectedfio = array(
            'lastname' => 'Ли',
            'firstname' => 'Дмитрий',
            'middlename' => 'Олегович'
        );
        $this->assertEquals($arformattedfio, $arexpectedfio);

        $arformattedfio = $uploaddatasync->separate_student_fio(
            "ИЛЬЯСОВ Мухаммед Байбосын-углы");
        $arexpectedfio = array(
            'lastname' => 'Ильясов',
            'firstname' => 'Мухаммед',
            'middlename' => 'Байбосын-углы'
        );
        $this->assertEquals($arformattedfio, $arexpectedfio);

        // В ФИО присутствует приставка.
        $arformattedfio = $uploaddatasync->separate_student_fio(
            "ЮНИСОВ Эльвин Мирджаган оглы");
        $arexpectedfio = array(
            'lastname' => 'Юнисов',
            'firstname' => 'Эльвин',
            'middlename' => 'Мирджаган оглы'
        );
        $this->assertEquals($arformattedfio, $arexpectedfio);

        $arformattedfio = $uploaddatasync->separate_student_fio(
            "САЛЕХ Лайт Б м");
        $arexpectedfio = array(
            'lastname' => 'Салех',
            'firstname' => 'Лайт',
            'middlename' => 'Б м'
        );
        $this->assertEquals($arformattedfio, $arexpectedfio);

        // Фамилия состоит из двух слов.
        $arformattedfio = $uploaddatasync->separate_student_fio(
            "ЭНРИКЕС ШИЯН Ярослав Эдгарович");
        $arexpectedfio = array(
            'lastname' => 'Энрикес Шиян',
            'firstname' => 'Ярослав',
            'middlename' => 'Эдгарович'
        );
        $this->assertEquals($arformattedfio, $arexpectedfio);

        $arformattedfio = $uploaddatasync->separate_student_fio(
            "АБУ САЛЕХ Руслан Аммар");
        $arexpectedfio = array(
            'lastname' => 'Абу Салех',
            'firstname' => 'Руслан',
            'middlename' => 'Аммар'
        );
        $this->assertEquals($arformattedfio, $arexpectedfio);

        // Регистронезависимый указатель ЯЯ* на иностранного студента.
        $arformattedfio = $uploaddatasync->separate_student_fio(
            "яя*ВЫПРИЦКАЯ Екатерина Александровна");
        $arexpectedfio = array(
            'lastname' => 'Выприцкая',
            'firstname' => 'Екатерина',
            'middlename' => 'Александровна'
        );
        $this->assertEquals($arformattedfio, $arexpectedfio);

        $arformattedfio = $uploaddatasync->separate_student_fio(
            "Яя*ВЫПРИЦКАЯ Екатерина Александровна");
        $arexpectedfio = array(
            'lastname' => 'Выприцкая',
            'firstname' => 'Екатерина',
            'middlename' => 'Александровна'
        );
        $this->assertEquals($arformattedfio, $arexpectedfio);

        $arformattedfio = $uploaddatasync->separate_student_fio(
            "яЯ*ВЫПРИЦКАЯ Екатерина Александровна");
        $arexpectedfio = array(
            'lastname' => 'Выприцкая',
            'firstname' => 'Екатерина',
            'middlename' => 'Александровна'
        );
        $this->assertEquals($arformattedfio, $arexpectedfio);

        $arformattedfio = $uploaddatasync->separate_student_fio(
            "ЯЯ*ВЫПРИЦКАЯ Екатерина Александровна");
        $arexpectedfio = array(
            'lastname' => 'Выприцкая',
            'firstname' => 'Екатерина',
            'middlename' => 'Александровна'
        );
        $this->assertEquals($arformattedfio, $arexpectedfio);

        // Двойная фамилия.
        $arformattedfio = $uploaddatasync->separate_student_fio(
            "СОКОЛОВА-ДОБРЕВА Дарья Ильинична");
        $arexpectedfio = array(
            'lastname' => 'Соколова-Добрева',
            'firstname' => 'Дарья',
            'middlename' => 'Ильинична'
        );
        $this->assertEquals($arformattedfio, $arexpectedfio);

        $arformattedfio = $uploaddatasync->separate_student_fio(
            "ЯЯ*АЛЬ-АШВАЛЬ Гадах ахмед Абдулфаттах");
        $arexpectedfio = array(
            'lastname' => 'Аль-Ашваль',
            'firstname' => 'Гадах',
            'middlename' => 'ахмед Абдулфаттах'
        );
        $this->assertEquals($arformattedfio, $arexpectedfio);

        $arformattedfio = $uploaddatasync->separate_student_fio(
            "ЯЯ*ЭССИЛФИ-АДДО Нана Кобина");
        $arexpectedfio = array(
            'lastname' => 'Эссилфи-Аддо',
            'firstname' => 'Нана',
            'middlename' => 'Кобина'
        );
        $this->assertEquals($arformattedfio, $arexpectedfio);

        // Отчество отсутствует.
        $arformattedfio = $uploaddatasync->separate_student_fio(
            "У ЦУЙЮЙ");
        $arexpectedfio = array(
            'lastname' => 'У',
            'firstname' => 'Цуйюй',
            'middlename' => ''
        );
        $this->assertEquals($arformattedfio, $arexpectedfio);

        $arformattedfio = $uploaddatasync->separate_student_fio(
            "СЕ Жуйлинь");
        $arexpectedfio = array(
            'lastname' => 'Се',
            'firstname' => 'Жуйлинь',
            'middlename' => ''
        );
        $this->assertEquals($arformattedfio, $arexpectedfio);

        // Отчество отсутствует, помечено - в разном количестве.
        $arformattedfio = $uploaddatasync->separate_student_fio(
            "ЯЯ*АНВАРИ Туфан -");
        $arexpectedfio = array(
            'lastname' => 'Анвари',
            'firstname' => 'Туфан',
            'middlename' => ''
        );
        $this->assertEquals($arformattedfio, $arexpectedfio);

        $arformattedfio = $uploaddatasync->separate_student_fio(
            "ЯЯ*ЛИЕДТКЕ Стефен ---");
        $arexpectedfio = array(
            'lastname' => 'Лиедтке',
            'firstname' => 'Стефен',
            'middlename' => ''
        );
        $this->assertEquals($arformattedfio, $arexpectedfio);

        $arformattedfio = $uploaddatasync->separate_student_fio(
            "ОУЯН Фукуань ------");
        $arexpectedfio = array(
            'lastname' => 'Оуян',
            'firstname' => 'Фукуань',
            'middlename' => ''
        );
        $this->assertEquals($arformattedfio, $arexpectedfio);

        // Отчество отсутствует, помечено = в разном количестве.
        $arformattedfio = $uploaddatasync->separate_student_fio(
            "ШВЕДУНЕНКО Александр =");
        $arexpectedfio = array(
            'lastname' => 'Шведуненко',
            'firstname' => 'Александр',
            'middlename' => ''
        );
        $this->assertEquals($arformattedfio, $arexpectedfio);

        $arformattedfio = $uploaddatasync->separate_student_fio(
            "ЯЯ*ЛИЕДТКЕ Стефен ===");
        $arexpectedfio = array(
            'lastname' => 'Лиедтке',
            'firstname' => 'Стефен',
            'middlename' => ''
        );
        $this->assertEquals($arformattedfio, $arexpectedfio);

        $arformattedfio = $uploaddatasync->separate_student_fio(
            "ОУЯН Фукуань ======");
        $arexpectedfio = array(
            'lastname' => 'Оуян',
            'firstname' => 'Фукуань',
            'middlename' => ''
        );
        $this->assertEquals($arformattedfio, $arexpectedfio);
    }

    /**
     * Идентифицирует студентов, вернувшихся из академического отпуска
     */
    public function test_identify_academics() {
        $uploaddatasync = new upload_data_sync();

        // Тест - один предмет с недопуском в прошлом году
        $arratings = array(
            "11207129" => array(
                "lastname" => "Голутвина",
                "firstname" => "Юлия",
                "middlename" => "Александровна",
                "email" => "std11207129@moodle.com",
                "group" => "ПРИН-466",
                "idnumber" => "11207129",
                "rating" => array(
                    "Защита информации" => "95",
                    "Исследование операций" => "97",
                    "Конструирование программного обеспечения" => "85",
                    "Математические основы искусственного интеллекта" => "76",
                    "Основы разработки WEB-приложений" => "85",
                    "Спецификация, архитектура и проектирование программных систем" => "95",
                    "Физическая культура" => "100",
                    "Эволюция и сопровождение программного обеспечения" => "70",
                    "Экономика программного обеспечения" => "94",
                    "Элективные курсы по физической культуре" => "100"
                )
            ),
            "11207133" => array(
                "lastname" => "Купцов",
                "firstname" => "Андрей",
                "middlename" => "Петрович",
                "email" => "std11207133@moodle.com",
                "group" => "ПРИН-466",
                "idnumber" => "11207133",
                "rating" => array(
                    "Защита информации" => "80",
                    "Исследование операций" => "95",
                    "Конструирование программного обеспечения" => "77",
                    "Математические основы искусственного интеллекта" => "76",
                    "Основы разработки WEB-приложений" => "80",
                    "Спецификация, архитектура и проектирование программных систем" => "70",
                    "Физическая культура" => "98",
                    "Эволюция и сопровождение программного обеспечения" => "68",
                    "Экономика программного обеспечения" => "94",
                    "Элективные курсы по физической культуре" => "98"
                )
            ),
            "11207134" => array(
                "lastname" => "Липов",
                "firstname" => "Максим",
                "middlename" => "Эдуардович",
                "email" => "std11207134@moodle.com",
                "group" => "ПРИН-466",
                "idnumber" => "11207134",
                "rating" => array(
                    "Защита информации" => "82",
                    "Исследование операций" => "61",
                    "Конструирование программного обеспечения" => "76",
                    "Математические основы искусственного интеллекта" => "70",
                    "Основы разработки WEB-приложений" => "70",
                    "Спецификация, архитектура и проектирование программных систем" => "80",
                    "Физическая культура" => "76",
                    "Эволюция и сопровождение программного обеспечения" => "не допущен",
                    "Экономика программного обеспечения" => "92",
                    "Элективные курсы по физической культуре" => "76"
                )
            )
        );

        $arstudents = array(
            "11307193" => array(
                "lastname" => "Маркосян",
                "firstname" => "Карен",
                "middlename" => "Гарникович",
                "email" => "std11307193@moodle.com",
                "group" => "ПРИН-467",
                "idnumber" => "11307193"
            ),
            "11207134" => array(
                "lastname" => "Липов",
                "firstname" => "Максим",
                "middlename" => "Эдуардович",
                "email" => "std11207134@moodle.com",
                "group" => "ПРИН-467",
                "idnumber" => "11207134"
            ),
            "11307199" => array(
                "lastname" => "Хомутцов",
                "firstname" => "Виктор",
                "middlename" => "Викторович",
                "email" => "std11307199@moodle.com",
                "group" => "ПРИН-467",
                "idnumber" => "11307199"
            )
        );

        $aracademics = array();
        $aracademics = $uploaddatasync->identify_academics($arratings, $arstudents);

        $this->assertEquals($aracademics, array("11207134"));

        // Тест - Несколько предметов с недопуском в прошлом году
        $arratings = array(
            "11207128" => array(
                "lastname" => "Броцкий",
                "firstname" => "Дмитрий",
                "middlename" => "Александрович",
                "email" => "std11207128@moodle.com",
                "group" => "ПРИН-466",
                "idnumber" => "11207128",
                "rating" => array(
                    "Защита информации" => "не допущен",
                    "Исследование операций" => "не допущен",
                    "Конструирование программного обеспечения" => "не допущен",
                    "Математические основы искусственного интеллекта" => "не допущен",
                    "Основы разработки WEB-приложений" => "не допущен",
                    "Спецификация, архитектура и проектирование программных систем" => "не допущен",
                    "Физическая культура" => "неявка",
                    "Эволюция и сопровождение программного обеспечения" => "не допущен",
                    "Экономика программного обеспечения" => "не допущен",
                    "Элективные курсы по физической культуре" => "неявка"
                )
            ),
            "11207129" => array(
                "lastname" => "Голутвина",
                "firstname" => "Юлия",
                "middlename" => "Александровна",
                "email" => "std11207129@moodle.com",
                "group" => "ПРИН-466",
                "idnumber" => "11207129",
                "rating" => array(
                    "Защита информации" => "95",
                    "Исследование операций" => "97",
                    "Конструирование программного обеспечения" => "85",
                    "Математические основы искусственного интеллекта" => "76",
                    "Основы разработки WEB-приложений" => "85",
                    "Спецификация, архитектура и проектирование программных систем" => "95",
                    "Физическая культура" => "100",
                    "Эволюция и сопровождение программного обеспечения" => "70",
                    "Экономика программного обеспечения" => "94",
                    "Элективные курсы по физической культуре" => "100"
                )
            ),
            "11207130" => array(
                "lastname" => "Емельянов",
                "firstname" => "Денис",
                "middlename" => "Владимирович",
                "email" => "std11207130@moodle.com",
                "group" => "ПРИН-466",
                "idnumber" => "11207130",
                "rating" => array(
                    "Защита информации" => "78",
                    "Исследование операций" => "91",
                    "Конструирование программного обеспечения" => "63",
                    "Математические основы искусственного интеллекта" => "80",
                    "Основы разработки WEB-приложений" => "80",
                    "Спецификация, архитектура и проектирование программных систем" => "80",
                    "Физическая культура" => "100",
                    "Эволюция и сопровождение программного обеспечения" => "61",
                    "Экономика программного обеспечения" => "92",
                    "Элективные курсы по физической культуре" => "100"
                )
            )
        );

        $arstudents = array(
            "11207128" => array(
                "lastname" => "Броцкий",
                "firstname" => "Дмитрий",
                "middlename" => "Александрович",
                "email" => "std11207128@moodle.com",
                "group" => "ПРИН-467",
                "idnumber" => "11207128"
            ),
            "11307193" => array(
                "lastname" => "Маркосян",
                "firstname" => "Карен",
                "middlename" => "Гарникович",
                "email" => "std11307193@moodle.com",
                "group" => "ПРИН-467",
                "idnumber" => "11307193"
            ),
            "11307199" => array(
                "lastname" => "Хомутцов",
                "firstname" => "Виктор",
                "middlename" => "Викторович",
                "email" => "std11307199@moodle.com",
                "group" => "ПРИН-467",
                "idnumber" => "11307199"
            )
        );

        $aracademics = array();
        $aracademics = $uploaddatasync->identify_academics($arratings, $arstudents);

        $this->assertEquals($aracademics, array("11207128"));

        // Тест - Несколько студентов, восстанавливающихся из академического отпуска
        $arratings = array(
            "11207134" => array(
                "lastname" => "Липов",
                "firstname" => "Максим",
                "middlename" => "Эдуардович",
                "email" => "std11207134@moodle.com",
                "group" => "ПРИН-466",
                "idnumber" => "11207134",
                "rating" => array(
                    "Защита информации" => "82",
                    "Исследование операций" => "61",
                    "Конструирование программного обеспечения" => "76",
                    "Математические основы искусственного интеллекта" => "70",
                    "Основы разработки WEB-приложений" => "70",
                    "Спецификация, архитектура и проектирование программных систем" => "80",
                    "Физическая культура" => "76",
                    "Эволюция и сопровождение программного обеспечения" => "не допущен",
                    "Экономика программного обеспечения" => "92",
                    "Элективные курсы по физической культуре" => "76"
                )
            ),
            "11207129" => array(
                "lastname" => "Голутвина",
                "firstname" => "Юлия",
                "middlename" => "Александровна",
                "email" => "std11207129@moodle.com",
                "group" => "ПРИН-466",
                "idnumber" => "11207129",
                "rating" => array(
                    "Защита информации" => "95",
                    "Исследование операций" => "97",
                    "Конструирование программного обеспечения" => "85",
                    "Математические основы искусственного интеллекта" => "76",
                    "Основы разработки WEB-приложений" => "85",
                    "Спецификация, архитектура и проектирование программных систем" => "95",
                    "Физическая культура" => "100",
                    "Эволюция и сопровождение программного обеспечения" => "70",
                    "Экономика программного обеспечения" => "94",
                    "Элективные курсы по физической культуре" => "100"
                )
            ),
            "11207130" => array(
                "lastname" => "Емельянов",
                "firstname" => "Денис",
                "middlename" => "Владимирович",
                "email" => "std11207130@moodle.com",
                "group" => "ПРИН-466",
                "idnumber" => "11207130",
                "rating" => array(
                    "Защита информации" => "78",
                    "Исследование операций" => "91",
                    "Конструирование программного обеспечения" => "63",
                    "Математические основы искусственного интеллекта" => "80",
                    "Основы разработки WEB-приложений" => "80",
                    "Спецификация, архитектура и проектирование программных систем" => "80",
                    "Физическая культура" => "100",
                    "Эволюция и сопровождение программного обеспечения" => "61",
                    "Экономика программного обеспечения" => "92",
                    "Элективные курсы по физической культуре" => "100"
                )
            ),
            "11207128" => array(
                "lastname" => "Броцкий",
                "firstname" => "Дмитрий",
                "middlename" => "Александрович",
                "email" => "std11207128@moodle.com",
                "group" => "ПРИН-466",
                "idnumber" => "11207128",
                "rating" => array(
                    "Защита информации" => "не допущен",
                    "Исследование операций" => "не допущен",
                    "Конструирование программного обеспечения" => "не допущен",
                    "Математические основы искусственного интеллекта" => "не допущен",
                    "Основы разработки WEB-приложений" => "не допущен",
                    "Спецификация, архитектура и проектирование программных систем" => "не допущен",
                    "Физическая культура" => "неявка",
                    "Эволюция и сопровождение программного обеспечения" => "не допущен",
                    "Экономика программного обеспечения" => "не допущен",
                    "Элективные курсы по физической культуре" => "неявка"
                )
            )
        );

        $arstudents = array(
            "11307193" => array(
                "lastname" => "Маркосян",
                "firstname" => "Карен",
                "middlename" => "Гарникович",
                "email" => "std11307193@moodle.com",
                "group" => "ПРИН-467",
                "idnumber" => "11307193"
            ),
            "11207134" => array(
                "lastname" => "Липов",
                "firstname" => "Максим",
                "middlename" => "Эдуардович",
                "email" => "std11207134@moodle.com",
                "group" => "ПРИН-467",
                "idnumber" => "11207134"
            ),
            "11307199" => array(
                "lastname" => "Хомутцов",
                "firstname" => "Виктор",
                "middlename" => "Викторович",
                "email" => "std11307199@moodle.com",
                "group" => "ПРИН-467",
                "idnumber" => "11307199"
            ),
            "11207128" => array(
                "lastname" => "Броцкий",
                "firstname" => "Дмитрий",
                "middlename" => "Александрович",
                "email" => "std11207128@moodle.com",
                "group" => "ПРИН-467",
                "idnumber" => "11207128"
            )
        );

        $aracademics = array();
        $aracademics = $uploaddatasync->identify_academics($arratings, $arstudents);

        $this->assertEquals($aracademics, array("11207134", "11207128"));
    }

    /**
     * Записать студентов в базу данных
     */
    public function test_write_students_to_db() {
        global $DB;
        $this->resetAfterTest(true);
        $table = 'enrol_studentssync_users';
        $uploaddatasync = new upload_data_sync();

        // Тест - добавление новых студентов
        $arstudents = array(
            "11307193" => array(
                "lastname" => "Маркосян",
                "firstname" => "Карен",
                "middlename" => "Гарникович",
                "email" => "std11307193@moodle.com",
                "group1" => "ПРИН-467",
                "idnumber" => "11307193",
                "isacademician" => "0",
                "ischangedgroup" => "0"
            ),
            "11207134" => array(
                "lastname" => "Липов",
                "firstname" => "Максим",
                "middlename" => "Эдуардович",
                "email" => "std11207134@moodle.com",
                "group1" => "ПРИН-467",
                "idnumber" => "11207134",
                "isacademician" => "1",
                "ischangedgroup" => "0"
            ),
            "11307199" => array(
                "lastname" => "Хомутцов",
                "firstname" => "Виктор",
                "middlename" => "Викторович",
                "email" => "std11307199@moodle.com",
                "group1" => "ПРИН-467",
                "idnumber" => "11307199",
                "isacademician" => "0",
                "ischangedgroup" => "0"
            )
        );

        $arexpectedstudents = array(
            "11307193" => array(
                "lastname" => "Маркосян",
                "firstname" => "Карен",
                "middlename" => "Гарникович",
                "email" => "std11307193@moodle.com",
                "group1" => "ПРИН-467",
                "idnumber" => "11307193",
                "isacademician" => "0",
                "ischangedgroup" => "0"
            ),
            "11207134" => array(
                "lastname" => "Липов",
                "firstname" => "Максим",
                "middlename" => "Эдуардович",
                "email" => "std11207134@moodle.com",
                "group1" => "ПРИН-467",
                "idnumber" => "11207134",
                "isacademician" => "1",
                "ischangedgroup" => "0"
            ),
            "11307199" => array(
                "lastname" => "Хомутцов",
                "firstname" => "Виктор",
                "middlename" => "Викторович",
                "email" => "std11307199@moodle.com",
                "group1" => "ПРИН-467",
                "idnumber" => "11307199",
                "isacademician" => "0",
                "ischangedgroup" => "0"
            )
        );

        $uploaddatasync->write_students_to_db($arstudents);

        $ardbstudents = $DB->get_records($table, null, '',
            'idnumber, lastname, firstname, middlename, email, group1, idnumber, isacademician, ischangedgroup');

        $this->assertEquals(convert_to_array($ardbstudents), $arexpectedstudents);

        // Тест - обновление студентов
        $arstudents = array(
            "11307193" => array(
                "lastname" => "Маркосян",
                "firstname" => "Карен",
                "middlename" => "Гарникович",
                "email" => "std11307193@moodle.com",
                "group1" => "ПРИН-466",
                "idnumber" => "11307193",
                "isacademician" => "0",
                "ischangedgroup" => "1"
            ),
            "11207134" => array(
                "lastname" => "Липов",
                "firstname" => "Максим",
                "middlename" => "Эдуардович",
                "email" => "std11207134@moodle.com",
                "group1" => "ПРИН-466",
                "idnumber" => "11207134",
                "isacademician" => "1",
                "ischangedgroup" => "1"
            )
        );

        $arexpectedstudents = array(
            "11307193" => array(
                "lastname" => "Маркосян",
                "firstname" => "Карен",
                "middlename" => "Гарникович",
                "email" => "std11307193@moodle.com",
                "group1" => "ПРИН-466",
                "idnumber" => "11307193",
                "isacademician" => "0",
                "ischangedgroup" => "1"
            ),
            "11207134" => array(
                "lastname" => "Липов",
                "firstname" => "Максим",
                "middlename" => "Эдуардович",
                "email" => "std11207134@moodle.com",
                "group1" => "ПРИН-466",
                "idnumber" => "11207134",
                "isacademician" => "1",
                "ischangedgroup" => "1"
            ),
            "11307199" => array(
                "lastname" => "Хомутцов",
                "firstname" => "Виктор",
                "middlename" => "Викторович",
                "email" => "std11307199@moodle.com",
                "group1" => "ПРИН-467",
                "idnumber" => "11307199",
                "isacademician" => "0",
                "ischangedgroup" => "0"
            )
        );

        $uploaddatasync->write_students_to_db($arstudents);

        $ardbstudents = $DB->get_records($table, null, '',
            'idnumber, lastname, firstname, middlename, email, group1, idnumber, isacademician, ischangedgroup');

        $this->assertEquals(convert_to_array($ardbstudents), $arexpectedstudents);
    }

    /**
     * Идентифицирует студентов, осуществивших перевод из одной группы в другую
     */
    public function test_identify_changed_group() {
        $uploaddatasync = new upload_data_sync();

        // Тест - студент осуществил перевод из одной группы в другую в семестрах текущего учебного года
        $arprevsemesterstudents = array(
            "11307193" => array(
                "lastname" => "Маркосян",
                "firstname" => "Карен",
                "middlename" => "Гарникович",
                "email" => "std11307193@moodle.com",
                "group1" => "ПРИН-467",
                "idnumber" => "11307193"
            ),
            "11207134" => array(
                "lastname" => "Липов",
                "firstname" => "Максим",
                "middlename" => "Эдуардович",
                "email" => "std11207134@moodle.com",
                "group1" => "ИВТ-462",
                "idnumber" => "11207134"
            ),
            "11307199" => array(
                "lastname" => "Хомутцов",
                "firstname" => "Виктор",
                "middlename" => "Викторович",
                "email" => "std11307199@moodle.com",
                "group1" => "ПРИН-467",
                "idnumber" => "11307199"
            )
        );

        $arcursemesterstudents = array(
            "11307193" => array(
                "lastname" => "Маркосян",
                "firstname" => "Карен",
                "middlename" => "Гарникович",
                "email" => "std11307193@moodle.com",
                "group1" => "ПРИН-467",
                "idnumber" => "11307193"
            ),
            "11207134" => array(
                "lastname" => "Липов",
                "firstname" => "Максим",
                "middlename" => "Эдуардович",
                "email" => "std11207134@moodle.com",
                "group1" => "ПРИН-467",
                "idnumber" => "11207134"
            ),
            "11307199" => array(
                "lastname" => "Хомутцов",
                "firstname" => "Виктор",
                "middlename" => "Викторович",
                "email" => "std11307199@moodle.com",
                "group1" => "ПРИН-467",
                "idnumber" => "11307199"
            )
        );

        $archangedgroup = array();
        $archangedgroup = $uploaddatasync->identify_changed_group($arprevsemesterstudents, $arcursemesterstudents);

        $this->assertEquals($archangedgroup, array("11207134"));

        // Тест - студент осуществил перевод из одной группы в другую в семестрах разных учебных годов
        $arprevsemesterstudents = array(
            "11207129" => array(
                "lastname" => "Голутвина",
                "firstname" => "Юлия",
                "middlename" => "Александровна",
                "email" => "std11207129@moodle.com",
                "group1" => "ПРИН-366",
                "idnumber" => "11207129"
            ),
            "11207130" => array(
                "lastname" => "Емельянов",
                "firstname" => "Денис",
                "middlename" => "Владимирович",
                "email" => "std11207130@moodle.com",
                "group1" => "ПРИН-366",
                "idnumber" => "11207130"
            ),
            "11207128" => array(
                "lastname" => "Броцкий",
                "firstname" => "Дмитрий",
                "middlename" => "Александрович",
                "email" => "std11207128@moodle.com",
                "group1" => "ПРИН-366",
                "idnumber" => "11207128"
            ),
            "11207134" => array(
                "lastname" => "Липов",
                "firstname" => "Максим",
                "middlename" => "Эдуардович",
                "email" => "std11207134@moodle.com",
                "group1" => "ИВТ-362",
                "idnumber" => "11207134"
            )
        );

        $arcursemesterstudents = array(
            "11207129" => array(
                "lastname" => "Голутвина",
                "firstname" => "Юлия",
                "middlename" => "Александровна",
                "email" => "std11207129@moodle.com",
                "group1" => "ПРИН-466",
                "idnumber" => "11207129"
            ),
            "11207130" => array(
                "lastname" => "Емельянов",
                "firstname" => "Денис",
                "middlename" => "Владимирович",
                "email" => "std11207130@moodle.com",
                "group1" => "ПРИН-466",
                "idnumber" => "11207130"
            ),
            "11207128" => array(
                "lastname" => "Броцкий",
                "firstname" => "Дмитрий",
                "middlename" => "Александрович",
                "email" => "std11207128@moodle.com",
                "group1" => "ПРИН-466",
                "idnumber" => "11207128"
            ),
            "11207134" => array(
                "lastname" => "Липов",
                "firstname" => "Максим",
                "middlename" => "Эдуардович",
                "email" => "std11207134@moodle.com",
                "group1" => "Ф-461",
                "idnumber" => "11207134"
            )
        );

        $archangedgroup = array();
        $archangedgroup = $uploaddatasync->identify_changed_group($arprevsemesterstudents, $arcursemesterstudents);

        $this->assertEquals($archangedgroup, array("11207134"));
    }
}