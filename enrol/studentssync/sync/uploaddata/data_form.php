<?php
/**
 * Created by PhpStorm.
 * User: mypc
 * Date: 27.05.2017
 * Time: 15:37
 */
defined('MOODLE_INTERNAL') || die();

require_once $CFG->libdir.'/formslib.php';

/**
 * Загрузка данных с сайта ВолгГТУ в CSV-файлы
 *
 * @copyright  2007 Petr Skoda  {@link http://skodak.org}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class admin_uploaddata_form extends moodleform {
    function definition () {
        $mform = $this->_form;

        $mform->addElement('static','header', get_string('upload_data', 'enrol_studentssync'));

        $mform->addElement('hidden', 'action', 'load');
        $mform->setType('action', PARAM_ALPHA);

        $this->add_action_buttons(false, get_string('upload_data_setup', 'enrol_studentssync'));
    }
}

class admin_syncstudents_form extends moodleform {
    function definition () {
        $mform = $this->_form;

        $mform->addElement('static','header', get_string('sync_students', 'enrol_studentssync'));

        $mform->addElement('hidden', 'action', 'load');
        $mform->setType('action', PARAM_ALPHA);

        $this->add_action_buttons(false, get_string('sync_students_setup', 'enrol_studentssync'));
    }
}