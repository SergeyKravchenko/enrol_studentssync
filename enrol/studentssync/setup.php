<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Adds new instance of enrol_studentssync to specified course.
 *
 * @package    enrol_studentssync
 * @copyright  2012 Petr Skoda {@link http://skodak.org}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require('../../config.php');
require_once("$CFG->dirroot/enrol/studentssync/setup_form.php");

$PAGE->set_url('/enrol/studentssync/setup.php');
$PAGE->set_pagelayout('admin');

$returnurl = new moodle_url('/admin/search.php');
if (!enrol_is_enabled('studentssync')) {
    redirect($returnurl);
}

$mform = new enrol_studentssync_setup_form();

if ($mform->is_cancelled()) {
    redirect($returnurl);
} else if ($data = $mform->get_data()) {
    $plugin = enrol_get_plugin('studentssync');
    $result = $plugin->sync(new \null_progress_trace());
}

$PAGE->set_title(get_string('pluginname', 'enrol_studentssync'));

echo $OUTPUT->header();
$mform->display();
echo $OUTPUT->footer();
