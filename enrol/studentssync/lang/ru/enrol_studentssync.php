<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'enrol_studentssync', language 'en'
 *
 * @package    enrol_studentssync
 * @copyright  2017, Sergey Kravchenko <kravchenko.sgv@gmail.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['pluginname'] = 'Запись студентов ВолгГТУ на курсы';
$string['pluginname_desc'] = 'Описание плагина';
$string['setup'] = 'Запустить';
$string['upload_data'] = 'Загрузка данных с сайта ВолгГТУ';
$string['upload_data_setup'] = 'Загрузить данные';
$string['sync_students'] = 'Синхронизация студентов';
$string['sync_students_setup'] = 'Запустить синхронизацию студентов';
$string['show_faculties'] = 'Факультеты, которые необходимо обработать';
$string['show_faculties_desc'] = 'Выберите те факультеты, над группами студентов которых необходимо производить обработку';
$string['show_faculties_112'] = 'Вечерний Кировский факультет';
$string['show_faculties_113'] = 'Красноармейский механико-металлургический факультет';
$string['show_faculties_114'] = 'Факультет автоматизированных систем, транспорта и вооружений';
$string['show_faculties_105'] = 'Факультет автомобильного транспорта';
$string['show_faculties_111'] = 'Факультет подготовки и переподготовки инженерных кадров';
$string['show_faculties_103'] = 'Факультет технологии конструкционных материалов';
$string['show_faculties_108'] = 'Факультет технологии пищевых производств';
$string['show_faculties_106'] = 'Факультет экономики и управления';
$string['show_faculties_107'] = 'Факультет электроники и вычислительной техники';
$string['show_faculties_102'] = 'Химико-технологический факультет';
$string['show_groups'] = 'Группы, которые необходимо обработать';
$string['show_groups_desc'] = 'Задайте регулярное выражение для тех групп, над которыми необходимо производить обработку.
 В качестве разделителя используйте ",". Пример такого регулярного выражения: "прин-46[6-7],ивт-26[0-3]".';
$string['insensitivity_show_groups'] = 'Регистронезависимость регулярного выражения опции "Группы, которые необходимо обработать"';
$string['insensitivity_show_groups_desc'] = 'Вы можете задать регистронезависимость регулярного выражения опции "Группы, которые необходимо обработать" или не учитывать её совсем.';