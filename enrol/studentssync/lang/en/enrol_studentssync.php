<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'enrol_studentssync', language 'en'
 *
 * @package    enrol_studentssync
 * @copyright  2017, Sergey Kravchenko <kravchenko.sgv@gmail.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['pluginname'] = 'Students to courses';
$string['pluginname_desc'] = 'Description';
$string['setup'] = 'Setup';
$string['upload_data'] = 'not found';
$string['upload_data_setup'] = 'not found';
$string['sync_students'] = 'not found';
$string['sync_students_setup'] = 'not found';
$string['show_faculties'] = 'not found';
$string['show_faculties_desc'] = 'not found';
$string['show_faculties_112'] = 'not found';
$string['show_faculties_113'] = 'not found';
$string['show_faculties_114'] = 'not found';
$string['show_faculties_105'] = 'not found';
$string['show_faculties_111'] = 'not found';
$string['show_faculties_103'] = 'not found';
$string['show_faculties_108'] = 'not found';
$string['show_faculties_106'] = 'not found';
$string['show_faculties_107'] = 'not found';
$string['show_faculties_102'] = 'not found';
$string['show_groups'] = 'not found';
$string['show_groups_desc'] = 'not found';
$string['insensitivity_show_groups'] = 'not found';
$string['insensitivity_show_groups_desc'] = 'not found';