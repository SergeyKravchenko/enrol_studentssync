<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Students sync enrolments plugin settings and presets.
 *
 * @package    enrol_studentssync
 * @copyright  2017 Sergey Kravchenko
 * @author     Sergey Kravchenko - based on code by Petr Skoda and others
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

if ($ADMIN->fulltree) {

    //--- general settings -----------------------------------------------------------------------------------
    $settings->add(new admin_setting_heading('enrol_studentssync_settings', '', get_string('pluginname_desc', 'enrol_studentssync')));

    // Факультеты, которые необходимо обработать
    $settings->add(new admin_setting_configmulticheckbox('enrol_studentssync/show_faculties',
        get_string('show_faculties', 'enrol_studentssync'),
        get_string('show_faculties_desc', 'enrol_studentssync'), array(), array(
            'show_faculties_112' => get_string('show_faculties_112', 'enrol_studentssync'),
            'show_faculties_113' => get_string('show_faculties_113', 'enrol_studentssync'),
            'show_faculties_114' => get_string('show_faculties_114', 'enrol_studentssync'),
            'show_faculties_105' => get_string('show_faculties_105', 'enrol_studentssync'),
            'show_faculties_111' => get_string('show_faculties_111', 'enrol_studentssync'),
            'show_faculties_103' => get_string('show_faculties_103', 'enrol_studentssync'),
            'show_faculties_108' => get_string('show_faculties_108', 'enrol_studentssync'),
            'show_faculties_106' => get_string('show_faculties_106', 'enrol_studentssync'),
            'show_faculties_107' => get_string('show_faculties_107', 'enrol_studentssync'),
            'show_faculties_102' => get_string('show_faculties_102', 'enrol_studentssync'),
        )
    ));

    // Группы, которые необходимо обработать
    $settings->add(new admin_setting_configtext('enrol_studentssync/show_groups',
        get_string('show_groups', 'enrol_studentssync'),
        get_string('show_groups_desc', 'enrol_studentssync'),
        '', PARAM_RAW, 60));

    // Регистронезависимость регулярного выражения опции "Группы, которые необходимо обработать"
    $settings->add(new admin_setting_configcheckbox('enrol_studentssync/insensitivity_show_groups',
        get_string('insensitivity_show_groups', 'enrol_studentssync'),
        get_string('insensitivity_show_groups_desc', 'enrol_studentssync'), 0));
}

$ADMIN->add('modules', new admin_externalpage('syncuploaddata', get_string('upload_data', 'enrol_studentssync'), "$CFG->wwwroot/enrol/studentssync/sync/uploaddata/index.php"));
